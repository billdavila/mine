/*
 Name: Billy Davila
 Class: CSC 21200-BC
 Assigment #2: Dynamic Bags
 Professor: Akshar Patel  
*/

#include <algorithm>    // Provides copy function
#include <cassert>       // Provides assert function
#include<iostream>      // Provides cin and cout
#include<vector>        // Provides vectors
using namespace std;


// Class of the dynamic Bag 
class bag
{
    public:
        // TYPEDEFS and MEMBER CONSTANTS
        typedef int value_type;
        typedef std::size_t size_type;
        static const size_type DEFAULT_CAPACITY = 30;        

        // CONSTRUCTORS and DESTRUCTOR
        bag(size_type initial_capacity = DEFAULT_CAPACITY) ; // constructor with a default parameter.
        bag(const bag& source); // the copy constructor
        ~bag( ); // the destructor
        // MODIFICATION MEMBER FUNCTIONS
        void reserve(size_type new_capacity);
        void insert(const value_type& entry);
        void operator +=(const bag& addend);
        void operator =(const bag& source);
        // CONSTANT MEMBER FUNCTIONS
        size_type size( ) const { return used; }
        size_type count(const value_type& target) const;
    	value_type return_number(size_t location) const { return data[location] ;}
	private:
        value_type *data;     // Pointer to partially filled dynamic array
        size_type used;       // How much of array is being used
        size_type capacity;   // Current capacity of the bag
};

const bag::size_type bag::DEFAULT_CAPACITY;

bag::bag(size_type initial_capacity)
{
        data = new value_type[initial_capacity];
        capacity = initial_capacity;
        used = 0;
}

// Copy constructor: allows to copy declare a bag an exact copy of an already existing one
bag::bag(const bag& source)
{
        data = new value_type[source.capacity]; // copy the data
        capacity = source.capacity;  // copy the capacity 
        used = source.used;  // copy the used variable
        copy(source.data, source.data + used, data); // copy all data[i] from that bag.
}

// destructor 
bag::~bag( )
{
	delete [ ] data; // once we are done, return the memory to the heap.
}

void bag::reserve(size_type new_capacity)
{
        value_type *larger_array;

        if (new_capacity == capacity)
            return; // There is no need to change the capacity since the allocated memory is 
			        // already the right size.

        if (new_capacity < used)
            new_capacity = used;  // We need to make sure that we don't change to a memory that 
			                    //   is less than we are using.

        larger_array = new value_type[new_capacity]; // get the new memory from the heap
        copy(data, data + used, larger_array); // copy or pass the data to the new array by using the copy function from
                                              // the library algorithm. 
        delete [ ] data;  // return the memory 
        data = larger_array;  // set the equal but with more memory
        capacity = new_capacity; // change the capacity.
}

// Member function that allows to insert an item in the bag 
void bag::insert(const value_type& entry)
{   
        if (used == capacity)
            reserve(used+1); // if it is full, add one more capacity and then insert the item.
        data[used] = entry;
        ++used;
}
    
void bag::operator +=(const bag& addend)
{
    if (used + addend.used > capacity)
            reserve(used + addend.used);
        
    copy(addend.data, addend.data + addend.used, data + used);
	used += addend.used;
}

// the assigment operator allows to safetly assign one bag member variable as equal
// as another one.
void bag::operator =(const bag& source)
{
	
	value_type *new_data;

	// Check for possible self-assignment:
	if (this == &source)
            return;

	// If needed, allocate an array with a different size:
	if (capacity != source.capacity)
	{ 
	    new_data = new value_type[source.capacity];
	    delete [ ] data;
	    data = new_data;
	    capacity = source.capacity;
	}

	// Copy the data from the source array:
	used = source.used;
	copy(source.data, source.data + used, data);
}

// member function that returns the ocurrence of a given value.
bag::size_type bag::count(const value_type& target) const
{
	size_type answer;
	size_type i;

	answer = 0;
	for ( i = 0; i < used; ++i)
	    if (target == data[i]) // if they are equal we add one to the answer.
		++answer;
	return answer;
}

// member function that can unite two bags 
bag operator +(const bag& b1, const bag& b2)
{
	bag answer(b1.size( ) + b2.size( )); // get the size by combining the two sizes.

	answer += b1; // now add the first bag to answer bag
	answer += b2; // also add the second bag to answer bag
	return answer;
}


int main()
{

	int n , q ; 
	
	bool flag = true ;
	
	// A while loop to ensure that the integers n and q are in the given constraint
	//if the user doesn't give integers between 1 and 10 it will print an error message.  
	while(flag)
	{
		
		cin >> n >> q ; 
		
		if ( n < 1  || q < 1 || n > 10 || q > 10  )
		{
			cout << "n and q must be integers between 1 and 10.\n" ; 
		}
		else
		{
			flag = false ; 
		}
	}
	
	
	bag mybags[n] ; // An array of dynamic bags
	
	
	int size, temp  ; 
	// nested loop to get the size of each bag and the each data[i]'s
	for(int i = 0  ; i < n ; ++i )
	{
		cin >> size ;  // get the size from the user. 
		mybags[i] = bag(size) ; // initialize each bag from the array
		
		for(int j = 0 ; j < size ; ++j)
		{
			cin >> temp ; // get the data[i] 
			mybags[i].insert(temp) ; // just insert it in the respected bag.
		}	
	}
	
	int pos1 ,pos2 ; 
	vector<int> v ;  
	
	// Now we can get the queries which are conformed of two positions. 
	for(int i = 0 ; i < q ; ++i )
	{
		cin >> pos1 >> pos2 ; // get the two positions
		temp = mybags[pos1].return_number(pos2)  ; // find the integers from those two positions
		v.push_back(temp) ; // store them in a vector.
	}
	
	for(int i = 0 ; i < q ; ++i )
	{
		 cout << v[i] << endl ; // finally , just display the result 
	}
	
	return 0 ; 
}


