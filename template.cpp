/*
 Name: Billy Davila
 Class: CSC 21200-BC
 Assigment #4: Class Templates
 Professor: Akshar Patel  
*/

#include<iostream>
#include<string> 
#include<vector>
#include<unistd.h>
#include<climits>



#include<stdlib.h>



using namespace std ;


/*
This is a template  class that can handle several underlying data types
such as int, float , string. If it is an int, float or a double it will 
add them up. In the case of a string it will concatenate them.
*/
template<class T> 
class AddElement
{
	public: 
		AddElement()
		{
			element1 = 0 ;
			element2 = 0 ; 
		}
		AddElement(T data1, T data2)
		{
			element1 = data1 ; 
			element2 = data2 ;  
		}
		T add()
		{
			return  (element1 + element2)  ; 
		}
		~AddElement()
		{
			
		}
	
	private:
		T element1 ; 
		T element2 ; 		
};

/*
Here we used the template specialization with the string. So it can 
concatenate the strings.
*/
template <>
class AddElement <string>{
	    public: 
	     AddElement()
	     {
	     	
		 }
		 AddElement(string data1 , string data2)
		 {
		 	one = data1 ; 
		 	two = data2 ; 
		 }
		string add()
		{
			string ans = one + two ; 
			 
		   return ans ;
		}
		~AddElement()
		{
			
		}
	private:
		string one ; 
		string two ;
	
};


void get_size(int& n)
{
	
	string mystring ;
	string temp = "" ; 
	
	getline(cin , mystring) ; 
	
	mystring += " " ; 
	double ans ;
	
	for(int i = 0 ; i < mystring.length() ; ++i )
	{
		
		if( mystring[i] != ' ' && mystring[i] != '\t' && ( isdigit(mystring[i]) || mystring[i] == '-' || mystring[i] == '.') ) 
		{
			temp += mystring[i] ; 
		}
		else
		{
			if(temp != "")
			{
				ans = atof(temp.c_str()) ; 
	
				temp = "" ; 
				break ; 
			}
		}
	}
	
	/*
		This will make sure that size it's actually an integer that 
		we can work with.
	*/
	int tem = ans ; 
	if(ans < 1 )
	{
		cout << "Invalid input, size should be at least one.\n" ;
	
		get_size(n) ; 
	}
	else if (tem != ans )
	{
		cout << "Invalid input, size should be an integer.\n" ;
		get_size(n) ;
	}
	else
	{
	   n = ans ;
	}
	
}


int main()
{
	int size ; 
	
	get_size(size) ; 
	
	
	string temp ;
	
	int counter = 0 ;
	
	string string1 , string2 , temp_string ; 
	int int1 , int2 , temp_int; 
	float float1, float2, temp_float ; 
	
	
	// Here we are going to store the answers by using vectors.
	vector<string > collec_string ; 
	vector<float > collec_num ;  
	
	
	/*
		Use a while loop that each time a user puts the right datatype we
		can work, we move the counter else we don't. If the user puts a string 
		we concatinate them and place them in the vector string. If the user puts 
		an integer or a float we just add them up and place them in the numbers vector.
		Each time they put the opposite we put an indicator to keep track of the order.
	*/
	while(counter < size)
	{
	   		cin >> temp ; 		      
	       if( temp == "string" || temp == "String")
		   {
				cin >> string1 >> string2 ;  
	      	  	
	      	  	AddElement<string> myelement(string1 , string2) ; 
	      	  	temp_string = myelement.add() ; 
				collec_string.push_back(temp_string) ; 
				collec_num.push_back(  INT_MAX   ) ;
				++counter ; 
		   }
			
	      	else if( temp == "int" || temp == "Int")
			{ 	 
					cin >> float1 >> float2 ; 
					
					int1 = float1 ;
					int2 = float2 ; 
					
					if( int1 == float1 && int2 == float2 )
					{
							AddElement<int> myelem(int1, int2) ;
							temp_int = myelem.add() ;	
							collec_num.push_back(temp_int) ; 
	      	  				collec_string.push_back("000") ;
	      	  				++counter ;
	      	  		}
	      	  		else
	      	  		{ 
	      	  		   cout << "Please provide integers.\n" ;  		  	
					}
	      	
			  
			}
	      	else if(temp == "float" || temp == "Float")
	      	{
	      		cin >> float1 >> float2 ; 
	      		AddElement<float> myele(float1, float2 ) ;
	      		collec_num.push_back(myele.add()) ; 
	      		collec_string.push_back("000") ;
				++counter ; 
			}
	      	else
	      	{
	      	 	cout << "Invalid Input.\n" ; 
			}
	}
	
 	
 	cout << endl ; 
 	
 	/*
 	 Here we print the answer in the order given 
 	*/
	for(int i = 0 ; i < size ; ++i )
	{
		  if(collec_string[i] != "000")
		    cout << collec_string[i] << endl ;
		   
		  if(collec_num[i] < INT_MAX)
		      cout << collec_num[i] << endl ;  	    
	}
	
	
	return 0 ; 
}


