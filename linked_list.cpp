/*
 Name: Billy Davila
 Class: CSC 21200-BC
 Assigment #3: Linked List
 Professor: Akshar Patel  
*/


#include<iostream>
#include<string>
#include<stdlib.h>


using namespace std ;

	/*
		There were multiple ways of doing it. I feel 
		more comfortable using a struct as a node 
		and then using a class separated class as a linked list.
	*/

struct node{
		/*
		A struct node that has an integer and 
		a pointer pointing to the next node
		*/
	int data ; 
	node* next ; 
};

class linked_list{
	/*
	 A Class that has two nodes as data variables one points to the head
	 of the linked list which is the beginning and the other one which acts
	 as a runner or cursor. 
	*/
	private:
		node* head , *runner ; 
	
	public:
		/*
		A default constructor that initialize each member variable
		as NULL pointers. 
		*/
	   linked_list(){
	   	    head = NULL ; 
	   	    runner = NULL ; 
	   }
	   /*
	   	Member function which is capable of adding a node at the end of a linked
	   	list. 
	   */
	  void add_node(const int& num)
	  {
	  	  node* next_node = new node() ; // create a new node  
	  	  next_node->data = num ; // set the data member as the argument num 
	  	  next_node->next = NULL ; // then send the pointer next member to point to the NULL pointer.
	  	  
	  	  if( head == NULL) // the linked list is empty.
	  	  {
	  	  	 head = next_node ; // set the head and runner as the next_node
	  	  	 runner = head ;    	
		  }
		  else // the linked list is not empty
		  {
		  	runner->next = next_node ; // set the runner pointer next to the next node  and then move the runner
		  	runner = runner->next ;   // to the last node of the linked list
		  }
	  }
	  
	  /*
	   A member function that can sort the linked list 
	   in an ascending order
	  */
	  void sort_list()
	  {
	  	 if(head == NULL) return ; // linked list is empty there is no need to sort the list. 
	  	
	 		int temp ; 
			bool flag ; 
			
			node* ptr ;
			node* last = NULL ; 
			/*
			 The loop works similar as bubble sort
			 it will rearrange it until all the greatest integer are
			 at the end of the linked list
			*/
			do{
				flag = false ; // assume the flag is false and the list is sorted. 
				ptr = head ; // set the pointer ptr to the beginning of the list. 
				while(ptr->next != last) //  
				{
					if( ptr->data > ptr->next->data) // there current node has a greater value than one from next. 
					{
						temp = ptr->data ;  
						ptr->data = ptr->next->data ; // change the data between them 
						ptr->next->data = temp ;   
						flag = true ; // keep doing the same until the if condition doesn't apply
					}
			       ptr = ptr->next ; 		// move the pointer
				}
				last = ptr ; 
				
			}while(flag) ;
			
	 }
	  	 	  
	  /*void add_them_sorted(const int& num)
	  {
	  	node* next_node = new node() ; 
	  	next_node->data = num ; 
	  	
	  	node* aux1 = head ; 
	  	node* aux2  ; 
	  	
	  	while( (aux1 != NULL )   && (aux1->data < num)  )
	  	{
	  		aux2 = aux1 ; 
	  		aux1 = aux1->next ; 
		}
	  	
	  	if(aux1 == head)
	  	{
	  		head = next_node ;
	  		next_node->next = aux1 ;
		}
		else
		{
			aux2->next = next_node ; 
			next_node->next = aux1  ;
		}	  	
	  }*/
	  
	  
	  /*
	  	A member function that removes duplicates from a linked list. 
	  	This will only work if and only if the linked list is sorted in 
	  	ascending order as a given.
	  */
	  void remove_duplicate()
	  {
	  	    if(head == NULL) return ; 
	  	
	  	
	  		node* current = head->next ; 	
	  	    node* previous = head ; 
	  	  while( current != NULL)
	  	  {
	  	  	   if(current->data == previous->data) // if they are  equal they are duplicates
	  	  	   {
	  	  	   	  previous->next = current->next ; // point to the next node and then remove it
	  	  	   	  delete current ; 
			      current = previous->next ;   	// send current to point to the next one.		 						  	  	   	
			   } 
			   else // there are no duplicates
			   {
			   	 previous = current ; // move both pointer to the next pointer
			   	 current = current->next ; 
			   }
		  }
	  	
	  }
	  
	  /*
	  A member function that will display the linked list 
	  so far. 
	  */
	  void display()
	  {
	  	  if(head == NULL) // list is empty 
	  	  {
	  	  	cout << "List is empty.\n" ;
	  	  	 return ; 
		  }
		   
		   node* temp = head ; // this will be our cursor pointer 
		   
		   while( temp != NULL)
		   {
		   	    if( temp->next == NULL ) // this is the last node 
		    	cout << temp->data << "\n" ;
		   	    else
		   	      cout << temp->data << "->" ;
			   
			    temp = temp->next ; // move the pointer
		   }
		   
	  }
	  /*
	    A member function that can delete the whole linked list 
	    and set the head to NULL. 
	  */
	  void erase_list()
	  {
	  	  if( head == NULL) return ; // there is nothing to do. 
	  	  
	  	  node* aux ; 
	  	  
	  	  while(head != NULL) // until the list is empty
	  	  {
	  	  	aux = head ; // set aux to head, move the head to the next one and then delete aux
	  	    head = head->next ;
			delete aux ;   	
		  }
	  }
	  /*
	  The destructor that free the memory used by the linked list 
	  and set the head and runner to point to NULL. 
	  */
	~linked_list() // the destructor which will
	{
		erase_list() ; // call the erase function 
		head = NULL ; // and set every member variable to NULL
		runner = NULL ;
	}
	
};

int main()
{
	int num ; 
	
	linked_list mylist ; 
	
	string mystring ;
	string temp = "" ; 
	
	
	getline(cin , mystring) ; 
	
	mystring += " " ; 
	
	/*
	Get the linked list as a string, we can get the data in a way that it will ignore any
	integer exclusively greater than 50  
	*/
	
	for( int i = 0 ; i < mystring.length() ; ++i )
	{
		    if( ( mystring[i] != ' ')  && (mystring[i] != '\t')  &&  ( isdigit(mystring[i]) || mystring[i] =='-' || mystring[i] == '.') )
		    {
		    	temp += mystring[i] ;
			}
			else
			{
				  if( temp != "")
				  {
						num = atoi( temp.c_str() ) ; // convert the string into an integer 	  	

						if(num  < 50)  mylist.add_node(num) ; // if the number given is less than 50 add a node to the linked list.
						
			
				  	    temp = "" ; 
				  }
			}
	 } 
	
	
	cout << "Entered list: " ; // display the list  without any modification
	mylist.display() ;
	
	
	/*
	 Sort , remove the duplicates from the linked list and then print it and 
	 erase it once done.
	*/
	mylist.sort_list() ; 
	
	
	mylist.remove_duplicate() ; 
	
	
	cout << "Modified list: " ; 
	mylist.display() ; 
	
	
		
	mylist.erase_list() ; 
	 
	return 0 ;
}



