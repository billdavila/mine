//============================================================================
// Name        : AssignmentTwo.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
//============================================================================

#include <iostream>
#include <array>
#include "header.h"
using namespace std;

int main() {
	dynamicBag run;
	run.getData();
	run.arrayData();
	run.queryData();
	run.display();
	run.deleteArray();
	return 0;
}
