/*
 Name: Billy Davila
 Class: CSC 21200-BC
 Assigment #1: Class
 Professor: Akshar Patel  
*/
#include<iostream>
#include<vector>
#include<cstdlib>
#include<cstdio>
#include<string>


#include<stdlib.h>

using namespace std ;

/*
Create a class called Employee with the given specifications.
I used vectors since they are easier to deal with than array. 
Also I included a constructor and destructor. Maybe did not 
really need them.
*/

class Employee{
	
	public:
		Employee() ;  
		void input() ; 
		int TotalSales() ;
		~Employee() ;   
	private:
	vector<int> sales ;
};

/*
 My default constructor fills all 5 product sales
 with zeros. 
*/
Employee::Employee()
{
	int num = 0 ; 
	
	for(size_t i = 0 ; i < 5 ; ++i )
	{
		sales.push_back(num) ; 
	}
}

/*
A class member function that can 
read five integers from the user
and stores them in the member variable vector sales
*/
void Employee::input()
{
	int num ; 
	
	bool flag = false ;
	
	sales.resize(0) ;
	
	
	string mystring ; 
	getline(cin , mystring) ; // We are going to get the data by using a string and use endline as
	                          // a way to continue from the user input. 
	
	string temp = "" ;
	
	vector<int > tempo ; // this is where we are going to store the integers.  
	
	mystring += " " ;
	for(int i = 0 ; i< mystring.length() ; ++i ) // read each character from the string line.
	{
		if( mystring[i] != ' ' && mystring[i] != '\t' && ( isdigit(mystring[i]) || mystring[i] == '-') ) 
		 // if it is not empty and if it is a digit or the minus sign then we read them and store them 
		 // in temp string variable. 
		{
			temp += mystring[i] ; // add it to the string variable. 
		}
		else
		{
			if(temp != "") // if it's not empty 
			{
				num = atoi(temp.c_str()) ; // convert it from string to an integer
				tempo.push_back(num) ;    // once we have the number, store it into the vector.  
				temp = "" ;              //  we have the number put the temp back as empty.
			}
		}
	}
	
	// The user must give 5 integers. 
	if(tempo.size() != 5)
	{
		cout << "Invalid Input. Please provide 5 integers.\n" ; 
		fflush(stdin) ; // empty the buffer
		input() ;  // start again, until we get those integers. 
	}
	else // we got those five integers
	{
	
			flag = false ; 
	        // Read all five integers to see if they all satisfy the given constraint.
			for( int i = 0 ; i < tempo.size() ; ++i )
			{
				if(tempo[i] < 0  ) // they are negative so it doesn't satisfy the constraint. 
				{
	 				flag = true ; // set the flag true 
	   				break  ; // break since one is enough to don't satisfy the condition. 
	   			}
	   
				if(tempo[i] > 50 ) // integers can't be greater than 50
				{
	    			flag = true ; // set the flag true
	       			break ; 
	    		}
			}

			if(flag) // this means the constrains were not meet
			{
		
				cout << "Product sales should be integers betweeen 0 and 50.Try again\n" ; // print an error message 
				fflush(stdin) ; // clear the buffer
		
				input() ; // And start all over again until we get the required conditions.
			}
            else // We satisfy all the conditions, now we can set them in the actual memmber variable vector.
            {
			
				for(size_t i = 0 ; i < 5 ; ++i )
				{
					num = tempo[i] ;   
	    			sales.push_back(num) ; 	
				}
				
			}
	}
}

/*
Another class member function that calculate 
the total of the 5 product sales and return it.
*/
int Employee::TotalSales()
{
	int sum =  0 ; // set sum equal to zero
	
	for(size_t i = 0 ; i < 5 ; ++i ) // read all five sales integers
	{
		sum += sales[i] ; // and add them to the variable sum
	}
	
	return sum ; // just return sum. 
}

/*
The destructor 
It doesn't do much since we are not using dynamic memory.
*/
Employee::~Employee()
{
	
}


void get_size(size_t& n)
{ 	 
    string mystring ; 
	string temp = "" ; 
	// First get the number of employees we are dealing with. 
	cout << "Enter the number of employees : ";
	getline(cin, mystring) ;
	
	mystring += " " ; // Same strategy as before.
	for(int i = 0 ; i < mystring.length() ; ++i)
	{
		if(mystring[i] != ' ' && mystring[i] != '\t' && (isdigit(mystring[i] ) || mystring[i] == '-' ) ) 
		{
			temp += mystring[i] ; 
		} 
		else
		{
		 	  if(  temp != "")
		 	  {
		 	  	n = atoi( temp.c_str() ) ;
				break ;  // we break since we only need one number.
			   }
		 }
	}

	/*
		Check if the number given make sense for a size, if not then start again.
		and make sure if satisfy the constraints.
	*/
	if( n > 100 || n <= 0)
	{
		system("CLS") ;
		cout << "Invalid Input." ;  
		cout << "The maximum capacity for employees is a 100 and there has to be at least one employee. Try again.\n" ; 
		get_size(n) ; 
	}
 
}


int main()
{
    size_t n  ;
	get_size(n) ; // get the size.
	
	// After that make an array of employees by asking 
	// a block of memory size of n.
	Employee myemployees[n] ; 
	
	// Keep track of Jack's total sales and
	// the other employees
	int jack = 0 ; 
	vector<int > competition ;
	int number ; 
	
	// Ask the user for the data and store them into 
	// our array of Employees while keeping track of 
	// Jack's total sales and the rest.
	for(size_t i = 0 ; i < n ; ++i)
	{
		fflush(stdin) ; // Empty the buffer. 
		cout << "Enter the sales of "<< i+1 << " Employee : " ; 
		myemployees[i].input() ; // get the input for each employee
	   
	     if(i == 0 ) // the index 0 is what Jack did as sales.
	     {
	     	jack = myemployees[i].TotalSales() ; 
		 }
		 else // the rest are from the other employees
		 {
		     number = myemployees[i].TotalSales() ;
	    	 competition.push_back(number) ;  // store the total sales in a vector from the other employees.
         }
	
	}
	
	// Now that we have the information, 
	// we need to keep track of how many employees
	// perform better than Jack. 
	int counter = 0 ; 
	
	for(size_t i = 0 ; i < competition.size() ; ++i )
	{
		 if( competition[i] > jack ) counter++ ; 		// if they have more in total sales, add one to counter.
	}
	
	cout << "Number of employees whose sales are greater than Jack : "<<  counter << endl ; // finally print the result. 
	
	
	return 0 ; 
}