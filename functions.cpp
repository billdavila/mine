/*
 * functions.cpp
 *
 *  Created on: Feb 7, 2020
 *      Author: nisha
 */

#include <iostream>
#include <array>
#include "header.h"
using namespace std;

void dynamicBag::getData() {
	getline(cin, input);
	firstSeperate();
	if((length <= 10 and length >= 0) and (queries >= 0 and queries <= 10)) {
		mainArray = new int*[length];
		insideArrayLength = new int[length];
		answers = new int[queries];
	} else {
		cout << "Invalid input please enter again!" << endl;
		getData();
	}
}

void dynamicBag::arrayData() {
	int count = 0;
	while(count != length) {
		getline(cin, input);
		secondSeperate();
		if(checkLength != arrayLength) {
			cout << "Invalid input!" << endl;
		} else {
			used++;
			count++;
		}
	}
}

void dynamicBag::queryData() {
	int count = 0;
	while(count != queries) {
		getline(cin, input);
		thirdSeperate();
		if ((iValue >= 0 and iValue <= length) and (jValue >= 0 and jValue <= insideArrayLength[iValue])) {
			answers[count++] = *(*(mainArray + iValue) + jValue);
		} else {
			cout << "Invalid Input!" << endl;
		}
	}
}

void dynamicBag::thirdSeperate() {
	string temp = "";
	bool check = true;
	int size = input.length();
	for(int i = 0; i < size; i++) {
		if (isspace(input.at(i)) or (i == (size - 1))) {
			if (i == (size - 1) and (input.at(i) != ' ')) {
				temp += input.at(i);
			}
			if(check and temp != "") {
				check = false;
				iValue = stoi(temp);
				temp = "";
			} else if (temp != ""){
				jValue = stoi(temp);
				temp = "";
			}
		} else {
			temp += input.at(i);
		}
	}
}

void dynamicBag::secondSeperate() {
	string temp = "";
	int counter = 0;
	checkLength = 0;
	bool check = true;
	int size = input.length();
	for(int i = 0; i < size; i++) {
		if (isspace(input.at(i)) or (i == (size - 1))) {
			if (i == (size - 1) and (input.at(i) != ' ')) {
				temp += input.at(i);
			}
			if(check and temp != "") {
				check = false;
				arrayLength = stoi(temp);
				mainArray[used] = new int[arrayLength];
				insideArrayLength[used] = arrayLength;
				temp = "";
			} else if (temp != "" ){
				*(*(mainArray + used) + counter) = stoi(temp);
				counter++;
				checkLength++;
				temp = "";
			}
		} else {
			temp += input.at(i);
		}
	}
}

// Reduce this function
void dynamicBag::firstSeperate() {
	string temp = "";
	bool check = true;
	int size = input.length();
	for(int i = 0; i < size; i++) {
		if (isspace(input.at(i)) or (i == (size - 1))) {
			if (i == (size - 1) and (input.at(i) != ' ')) {
				temp += input.at(i);
			}
			if(check and temp != "") {
				check = false;
				length = stoi(temp);
				temp = "";
			} else if (temp != "") {
				queries = stoi(temp);
				temp = "";
			}
		} else {
			temp += input.at(i);
		}
	}
}

void dynamicBag::display() {
	for (int i = 0; i < queries; i++) {
		cout << *(answers + i) << endl;
	}
}

void dynamicBag::deleteArray() {
	delete[] mainArray;
	delete[] insideArrayLength;
	delete[] answers;
}