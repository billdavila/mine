/*
 * header.h
 *
 *  Created on: Feb 1, 2020
 *      Author: nisha
 */

#ifndef HEADER_H_
#define HEADER_H_

#include <iostream>
using namespace std;

class dynamicBag {
private:
	int **mainArray; // Main Array
	int *insideArrayLength; // Inside Array Length Keeper
	int *answers;
	int queries;
	int iValue;
	int jValue;
	int used = 0;
	int length; // Main Array Length
	int checkLength = 0;
	int arrayLength; // Inside Array Length
	string input;
public:
	void firstSeperate();
	void secondSeperate();
	void thirdSeperate();
	void getData();
	void arrayData();
	void queryData();
	void display();
	void deleteArray();
};



#endif /* HEADER_H_ */
